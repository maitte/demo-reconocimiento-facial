import cv2 
import time

faceCascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

videoCapture = cv2.VideoCapture(0)

faceDetectedTime = None
faceDetectedDuration = 1 # In seconds

while True:
    # Capture frame-by-frame
    ret, frame = videoCapture.read()
    
    # Convert the frame to grayscale for face detection
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Detect faces in the grayscale frame
    faces = faceCascade.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=5, minSize=(30, 30))

    # Draw a rectangle around each face and display the image
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
        
        # Check if face has been detected for more than the specified duration
        if faceDetectedTime is None:
            faceDetectedTime = time.time()
        elif time.time() - faceDetectedTime >= faceDetectedDuration:
            # Save the image when the face has been detected for more than 1 second
            cv2.imwrite('face_detected2.jpg', frame)
            print('Face detected and saved.')
            # Reset the face detection timer
            faceDetectedTime = None

    # Display the resulting frame
    cv2.imshow('Video', frame)
    
    # Quit the program when 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the video capture and close all windows
videoCapture.release()
cv2.destroyAllWindows()