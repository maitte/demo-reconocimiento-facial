function validateForm() {
    const username = document.getElementById("username").value.trim();
    const email = document.getElementById("email").value.trim();
    const password = document.getElementById("password").value;
    const confirmPassword = document.getElementById("confirm-password").value;
    const photo = document.getElementById("photo").files[0];
  
    // Check if username is at least 3 characters long
    if (username.length < 3) {
      alert("Username must be at least 3 characters long");
      return false;
    }
  
    // Check if email is valid
    if (!/\S+@\S+\.\S+/.test(email)) {
      alert("Email is not valid");
      return false;
    }
  
    // Check if password is at least 8 characters long
    if (password.length < 8) {
      alert("Password must be at least 8 characters long");
      return false;
    }
  
    // Check if password and confirm password match
    if (password !== confirmPassword) {
      alert("Passwords do not match");
      return false;
    }
  
    // Send the photo to the Python backend for facial recognition
    const formData = new FormData();
    formData.append("photo", photo);
  
    fetch('/backend/register', {
      method: 'POST',
      body: formData
    })
    .then(response => {
      if (response.ok) {
        return response.json();
      }
      throw new Error("Network response was not ok");
    })
    .then(json => {
      if (json.success) {
        alert("Registration successful");
        return true;
      } else {
        alert("Facial recognition failed");
        return false;
      }
    })
    .catch(error => {
      console.error("Error:", error);
      alert("An error occurred while processing your request");
      return false;
    });
  
    // Prevent the form from submitting before the facial recognition is complete
    return false;
  }
  